package nl.utwente.di.temperature;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTemperatureConverter {
    @Test
    public void test0C() {
        assertEquals(TemperatureConverter.toFahrenheit(0), 32);
    }

    @Test
    public void test100C() {
        assertEquals(TemperatureConverter.toFahrenheit(100), 212);
    }

    @Test
    public void testNegative100C() {
        assertEquals(TemperatureConverter.toFahrenheit(-100), -148);
    }
}
