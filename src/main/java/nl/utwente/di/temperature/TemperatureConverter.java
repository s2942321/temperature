package nl.utwente.di.temperature;

public class TemperatureConverter {

    /**
     * Convert Celsius to the equivalent temperature in Fahrenheit.
     * @param temp Temperature in celsius
     * @return Temperature in Fahrenheit
     */
    public static double toFahrenheit(double temp) {
        return temp * 9/5 + 32;
    }
}
